#include "Magmast/EspNow/Address.h"

#include <etl/string_stream.h>

const Magmast::EspNow::Address Magmast::EspNow::Address::BROADCAST{
    {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}};

Magmast::EspNow::Address::Address(const_pointer bytes)
{
    std::copy(bytes, bytes + SIZE, _bytes.begin());
}

Magmast::EspNow::Address::Address(const etl::array_view<value_type> &bytes)
{
    std::copy(bytes.begin(), bytes.end(), _bytes.begin());
}

Magmast::EspNow::Address::Address(const std::array<value_type, SIZE> &&bytes)
    : _bytes{std::move(bytes)}
{
}

Magmast::EspNow::Address::Address(const etl::string_view &str)
{
    for (auto byteIndex = 0; byteIndex < SIZE; ++byteIndex)
    {
        const etl::string<2> byteStr{str.data() + byteIndex * 3, 2};
        _bytes[byteIndex] = std::strtol(byteStr.c_str(), nullptr, 16);
    }
}

Magmast::EspNow::Address::iterator Magmast::EspNow::Address::begin()
{
    return _bytes.begin();
}

Magmast::EspNow::Address::iterator Magmast::EspNow::Address::end()
{
    return _bytes.end();
}

Magmast::EspNow::Address::const_iterator Magmast::EspNow::Address::cbegin() const
{
    return _bytes.cbegin();
}

Magmast::EspNow::Address::const_iterator Magmast::EspNow::Address::cend() const
{
    return _bytes.cend();
}

Magmast::EspNow::Address::pointer Magmast::EspNow::Address::data()
{
    return _bytes.data();
}

Magmast::EspNow::Address::const_pointer Magmast::EspNow::Address::data() const
{
    return _bytes.data();
}

const Magmast::EspNow::Address::value_type &
Magmast::EspNow::Address::operator[](size_type index) const
{
    return _bytes[index];
}

Magmast::EspNow::Address::value_type &
Magmast::EspNow::Address::operator[](size_type index)
{
    return _bytes[index];
}

constexpr Magmast::EspNow::Address::size_type
Magmast::EspNow::Address::size() const
{
    return _bytes.size();
}

etl::string<17> Magmast::EspNow::Address::toString() const
{
    etl::string<17> str;

    etl::string_stream stream{str};
    stream << etl::hex << etl::setfill('0');

    for (auto byte = cbegin(); byte != cend(); ++byte)
    {
        stream << etl::setw(2)
               << *byte
               << etl::setw(1)
               << ":";
    }

    return str;
}

bool operator==(
    const Magmast::EspNow::Address &lhs,
    const Magmast::EspNow::Address &rhs)
{
    return std::equal(lhs.cbegin(), lhs.cend(), rhs.cbegin());
}