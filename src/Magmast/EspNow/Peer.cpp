#include "Magmast/EspNow/Peer.h"

#include <Arduino.h>

const Magmast::EspNow::Peer Magmast::EspNow::Peer::BROADCAST{
    Magmast::EspNow::Address({0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF})};

Magmast::EspNow::Peer::Peer(const Address &&address)
    : _address{std::move(address)}
{
}

const Magmast::EspNow::Address &
Magmast::EspNow::Peer::getAddress() const
{
    return _address;
}

Magmast::EspNow::Peer::operator esp_now_peer_info_t() const
{
    esp_now_peer_info_t peer;
    std::copy(_address.cbegin(), _address.cend(), peer.peer_addr);
    peer.channel = 0;
    peer.encrypt = false;
#ifdef ARDUINO2
    peer.ifidx = wifi_interface_t::WIFI_IF_STA;
#else
    peer.ifidx = WIFI_IF_STA;
#endif
    return peer;
}