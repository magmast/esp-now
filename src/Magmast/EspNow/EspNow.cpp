#include "Magmast/EspNow/EspNow.h"

#include <WiFi.h>

#ifndef ESP_NOW_THROTTLE_MS
#define ESP_NOW_THROTTLE_MS 20
#endif

static bool setupWiFi()
{
    const auto currentMode = WiFi.getMode();

    // WiFi mode isn't currently set to anything so I suppose the caller
    // wouldn't mind, if we'll set it to our liking.
    if (currentMode == WIFI_MODE_NULL)
    {
        WiFi.mode(WIFI_MODE_STA);
        return true;
    }

    // WiFi mode is set, but it isn't in the STA/APSTA mode and that's bad.
    // We'll not change it as the caller already set it to something and
    // changing it here could be suprising and cause hard to track bugs. Instead
    // we'll just return `false` to indicate the error.
    if (currentMode & WIFI_MODE_STA != WIFI_MODE_STA)
    {
        return false;
    }

    // If none of the previous if statements were executed, then WiFi is already
    // set properly and we can just return `true` to indicate success.
    return true;
}

Magmast::EspNow::EspNow Magmast::EspNow::EspNow::_instance;

void Magmast::EspNow::EspNow::_handleReceive(
    const uint8_t *mac, const uint8_t *data, const int size)
{
    if (getInstance()._receiveCallback)
    {
        (*getInstance()._receiveCallback)(
            Address(mac),
            etl::array_view<uint8_t>(data, size));
    }
}

void Magmast::EspNow::EspNow::_handleSend(
    const uint8_t *mac, const esp_now_send_status_t status)
{
    getInstance()._isSending = false;
}

Magmast::EspNow::EspNow &Magmast::EspNow::EspNow::getInstance()
{
    return _instance;
}

bool Magmast::EspNow::EspNow::begin() const
{
    if (!setupWiFi())
    {
        return false;
    }

    auto err = esp_now_init();
    if (err != ESP_OK)
    {
        return false;
    }

    err = esp_now_register_recv_cb(&EspNow::_handleReceive);
    if (err != ESP_OK)
    {
        return false;
    }

    err = esp_now_register_send_cb(&EspNow::_handleSend);
    return err == ESP_OK;
}

bool Magmast::EspNow::EspNow::send(
    const Peer &peer, const etl::array_view<uint8_t> &payload)
{
    return _send(peer, payload.data(), payload.size());
}

bool Magmast::EspNow::EspNow::broadcast(const etl::array_view<uint8_t> &payload)
{
    return send(Peer::BROADCAST, payload);
}

bool Magmast::EspNow::EspNow::_send(
    const Peer &peer, const uint8_t *payload, const size_t size)
{
    if (_isSending)
    {
        return false;
    }

    if (!esp_now_is_peer_exist(peer.getAddress().data()))
    {
        if (!_addPeer(peer))
        {
            return false;
        }
    }

    _throttle();

    const auto err = esp_now_send(peer.getAddress().data(), payload, size);
    if (err != ESP_OK)
    {
        return false;
    }

    _isSending = true;

    return true;
}

void Magmast::EspNow::EspNow::_throttle()
{
#ifndef ESP_NOW_NO_THROTTLING
    return;
#endif

    const auto now = millis();
    if (_lastSentTime != 0 && now - _lastSentTime < ESP_NOW_THROTTLE_MS)
    {
        delay(now - _lastSentTime);
    }
    _lastSentTime = now;
}

void Magmast::EspNow::EspNow::onReceive(
    const ReceiveCallback &&callback)
{
    // We don't directly register callback with `esp_now_register_recv_cb`,
    // because thanks to using `std::function` user of this class is allowed
    // to register a member function etc. instead of just plain function or
    // static member function.
    _receiveCallback = callback;
}

void Magmast::EspNow::EspNow::removeOnReceive()
{
    _receiveCallback = etl::nullopt;
}

bool Magmast::EspNow::EspNow::_addPeer(const Peer &peer) const
{
    const auto peerInfo = static_cast<esp_now_peer_info_t>(peer);
    const auto err = esp_now_add_peer(&peerInfo);
    return err == ESP_OK;
}