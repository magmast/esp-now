#include <unity.h>

#include <Magmast/EspNow/Address.h>

using Address = Magmast::EspNow::Address;

void test_Address_StringViewConstructor_zeroed()
{
    const auto address = Address("00:00:00:00:00:00");
    const std::array<uint8_t, 6> expected{0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected.data(), address.data(), 6);
}

void test_Address_StringViewConstructor_broadcast()
{
}

void setup()
{
    UNITY_BEGIN();

    RUN_TEST(test_Address_StringViewConstructor_zeroed);
    RUN_TEST(test_Address_StringViewConstructor_broadcast);

    UNITY_END();
}

void loop()
{
}