#ifndef MAGMAST_ESP_NOW_PEER_H
#define MAGMAST_ESP_NOW_PEER_H

#include <esp_now.h>

#include <array>

#include "Address.h"

namespace Magmast
{
    namespace EspNow
    {
        class Peer
        {
        public:
            static const Peer BROADCAST;

            explicit Peer(const Address &&address);

            const Address &getAddress() const;

            explicit operator esp_now_peer_info_t() const;

        private:
            Address _address;
        };
    }
}

#endif