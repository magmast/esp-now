#ifndef MAGMAST_ESP_NOW_ADDRESS_H
#define MAGMAST_ESP_NOW_ADDRESS_H

#include <cstddef>

#include <array>

#include <etl/array_view.h>
#include <etl/string.h>

namespace Magmast
{
    namespace EspNow
    {
        class Address
        {
        public:
            static constexpr const size_t SIZE = 6;

            using value_type = std::array<uint8_t, SIZE>::value_type;
            using iterator = std::array<uint8_t, SIZE>::iterator;
            using const_iterator = std::array<value_type, SIZE>::const_iterator;
            using pointer = std::array<value_type, SIZE>::pointer;
            using const_pointer = std::array<value_type, SIZE>::const_pointer;
            using size_type = size_t;

            static const Address BROADCAST;

            Address() = default;
            explicit Address(const_pointer bytes);
            explicit Address(const etl::array_view<value_type> &bytes);
            explicit Address(const std::array<value_type, SIZE> &&bytes);
            explicit Address(const etl::string_view &str);

            pointer data();
            const_pointer data() const;

            iterator begin();
            iterator end();

            const_iterator cbegin() const;
            const_iterator cend() const;

            const value_type &operator[](size_type index) const;
            value_type &operator[](size_type index);

            constexpr size_type size() const;

            etl::string<17> toString() const;

        private:
            std::array<value_type, SIZE> _bytes;
        };
    }
}

bool operator==(const Magmast::EspNow::Address &lhs, const Magmast::EspNow::Address &rhs);

#endif