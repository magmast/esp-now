#ifndef MAGMAST_ESP_NOW_H
#define MAGMAST_ESP_NOW_H

#include "Magmast/EspNow/EspNow.h"

using EspNowPeer = Magmast::EspNow::Peer;
using EspNowAddress = Magmast::EspNow::Address;
using EspNowPaylod = etl::array_view<uint8_t>;

extern Magmast::EspNow::EspNow &EspNow;

#endif